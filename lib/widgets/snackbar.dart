import 'package:flutter/material.dart';

void snackBar(BuildContext context, IconData icon, String text) {
  ScaffoldMessenger.of(context).removeCurrentSnackBar();
  ScaffoldMessenger.of(context).showSnackBar(
    SnackBar(
      elevation: 3,
      content: Row(
        children: [
          Padding(
            padding: const EdgeInsets.only(right: 10),
            child: Icon(
              icon,
              color: Colors.amber,
            ),
          ),
          Text(
            text,
            style: Theme.of(context).textTheme.bodyText1!.copyWith(color: Theme.of(context).backgroundColor),
          ),
        ],
      ),
      backgroundColor: Theme.of(context).textTheme.bodyText2!.color,
    ),
  );
}
