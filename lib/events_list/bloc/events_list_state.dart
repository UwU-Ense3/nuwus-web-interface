part of 'events_list_bloc.dart';

@immutable
abstract class EventsListState {}

class EventsListInitial extends EventsListState {}

class EventsListLoadInProgress extends EventsListState {
  EventsListLoadInProgress();
}

class EventsListLoadSuccess extends EventsListState {
  final List<Event> eventsList;
  final List<Tag> tagsList;
  final VisibilityFilter filter;

  EventsListLoadSuccess(this.eventsList, this.tagsList, this.filter);
}

class EventsListHasError extends EventsListState {
  final Exception stackTrace;

  EventsListHasError(this.stackTrace);
}
