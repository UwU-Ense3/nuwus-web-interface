import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:nuwus_web_interface/repository/event_repository.dart';
import 'package:nuwus_web_interface/ressources/event.dart';

import 'visibility_filter.dart';

part 'events_list_event.dart';

part 'events_list_state.dart';

class EventsListBloc extends Bloc<EventsListEvent, EventsListState> {
  EventsListBloc() : super(EventsListInitial()) {
    on<ListUpdateRequested>(_onListUpdateRequested);
    on<FilterUpdateRequested>(_onFilterUpdateRequested);
    on<SortingUpdateRequested>(_onSortingUpdateRequested);
  }

  final _eventRepo = EventRepository();
  final _tagRepo = TagRepository();
  List<Event> eventList = [];
  List<Tag> tagList = [];
  VisibilityFilter filter = VisibilityFilter(
    showPast: false,
    rawSearchQuery: '',
    sortOrder: SortOrder.chronological,
    selectedTags: [],
  ); // Default filter

  void _onListUpdateRequested(ListUpdateRequested event, Emitter<EventsListState> emit) async {
    emit(EventsListLoadInProgress());
    try {
      eventList = await _eventRepo.fetchEventsList(hidePast: false);
    } on Exception catch (e, stack) {
      print("Erreur de connexion : $e");
      print(stack);
      emit(EventsListHasError(e));
      return;
    }
    try {
      tagList = await _tagRepo.fetchTagsList();
    } on Exception catch (e, stack) {
      print("Erreur de connexion : $e");
      print(stack);
      emit(EventsListHasError(e));
      return;
    }
    var filteredList = filter.applyFilteringTo(eventList);
    emit(EventsListLoadSuccess(filteredList, tagList, filter));
  }

  void _onFilterUpdateRequested(FilterUpdateRequested event, Emitter<EventsListState> emit) async {
    if (state is EventsListInitial || state is EventsListHasError || state is EventsListLoadInProgress) {
      emit(EventsListLoadInProgress());
      try {
        eventList = await _eventRepo.fetchEventsList(hidePast: false);
      } on Exception catch (e, stack) {
        print("Erreur de connexion : $e");
        print(stack);
        emit(EventsListHasError(e));

        return;
      }
      try {
        tagList = await _tagRepo.fetchTagsList();
      } on Exception catch (e, stack) {
        print("Erreur de connexion : $e");
        print(stack);
        emit(EventsListHasError(e));

        return;
      }
    }
    filter = filter.copyWith(
      showPast: event.showPast,
      rawSearchQuery: event.searchQuery,
      selectedTags: event.selectedTags,
    );
    var filteredList = filter.applyFilteringTo(eventList);
    emit(EventsListLoadSuccess(filteredList, tagList, filter));
  }

  void _onSortingUpdateRequested(SortingUpdateRequested event, Emitter<EventsListState> emit) async {
    if (state is EventsListInitial || state is EventsListHasError || state is EventsListLoadInProgress) {
      emit(EventsListLoadInProgress());
      try {
        eventList = await _eventRepo.fetchEventsList(hidePast: false);
      } on Exception catch (e, stack) {
        print("Erreur de connexion : $e");
        print(stack);
        emit(EventsListHasError(e));
        return;
      }
      try {
        tagList = await _tagRepo.fetchTagsList();
      } on Exception catch (e, stack) {
        print("Erreur de connexion : $e");
        print(stack);
        emit(EventsListHasError(e));

        return;
      }
    }
    filter = filter.copyWith(sortOrder: event.sortOrder);
    filter.applyOrderingTo(eventList);
    var filteredList = filter.applyFilteringTo(eventList);
    emit(EventsListLoadSuccess(filteredList, tagList, filter));
  }
}
