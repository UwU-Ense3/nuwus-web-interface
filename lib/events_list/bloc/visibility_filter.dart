import 'package:diacritic/diacritic.dart';
import 'package:nuwus_web_interface/ressources/event.dart';

enum SortOrder {
  chronological,
  reverseChronological,
  alphabetical,
  reverseAlphabetical,
  increasingPrice,
  decreasingPrice,
}

class VisibilityFilter {
  final bool showPast;
  String searchQuery = '';
  final SortOrder sortOrder;
  final List<Tag> selectedTags;

  VisibilityFilter(
      {required this.selectedTags,
      required this.showPast,
      required String rawSearchQuery,
      required this.sortOrder}) {
    searchQuery = removeDiacritics(rawSearchQuery.toLowerCase());
  }

  bool _filterElement(Event element) {
    if (element.endTime.isBefore(element.beginTime)) {
      // print('Erreur dans l\'événement ${element.name} : débute le ${element.beginTime} et finit le ${element.endTime}');
    }
    if (!showPast) {
      if (!element.endTime.isAfter(DateTime.now())) {
        return false;
      }
    }
    if (searchQuery != null && searchQuery != '') {
      if (!removeDiacritics(element.name.toLowerCase()).contains(searchQuery)) {
        return false;
      }
    }
    if (selectedTags != []) {
      for (var tag in selectedTags) {
        if (!element.tags.contains(tag)) {
          return false;
        }
      }
    }
    return true;
  }

  List<Event> applyFilteringTo(List<Event> list) {
    return list.where(_filterElement).toList();
  }

  int _compareDate(Event elementA, Event elementB) {
    return elementA.beginTime.compareTo(elementB.beginTime);
  }

  int _compareName(Event elementA, Event elementB) {
    return removeDiacritics(elementA.name.toLowerCase())
        .compareTo(removeDiacritics(elementB.name.toLowerCase()));
  }

  int _comparePrice(Event elementA, Event elementB) {
    if (elementA.price == null && elementB == null) {
      return 0;
    }
    if (elementA.price == null) {
      return -1;
    }
    if (elementB.price == null) {
      return 1;
    }
    return elementA.price.compareTo(elementB.price);
  }

  void applyOrderingTo(List<Event> list) {
    if (sortOrder == SortOrder.chronological) {
      list.sort(_compareDate);
    } else if (sortOrder == SortOrder.reverseChronological) {
      list.sort((a, b) => -_compareDate(a, b)); // opposé de la comparaison : ordre inverse
    } else if (sortOrder == SortOrder.alphabetical) {
      list.sort(_compareName);
    } else if (sortOrder == SortOrder.reverseAlphabetical) {
      list.sort((a, b) => -_compareName(a, b)); // opposé de la comparaison : ordre inverse
    } else if (sortOrder == SortOrder.increasingPrice) {
      list.sort(_comparePrice);
    } else if (sortOrder == SortOrder.decreasingPrice) {
      list.sort((a, b) => -_comparePrice(a, b)); // opposé de la comparaison : ordre inverse
    }
  }

  VisibilityFilter copyWith({
    bool? showPast,
    String? rawSearchQuery,
    SortOrder? sortOrder,
    List<Tag>? selectedTags,
  }) {
    return VisibilityFilter(
      showPast: showPast ?? this.showPast,
      rawSearchQuery: rawSearchQuery ?? this.searchQuery,
      sortOrder: sortOrder ?? this.sortOrder,
      selectedTags: selectedTags ?? this.selectedTags,
    );
  }
}
