import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_markdown/flutter_markdown.dart';
import 'package:intl/intl.dart';
import 'package:nuwus_web_interface/auth/bloc/login_bloc.dart';
import 'package:nuwus_web_interface/event_creator/cubit/event_cubit.dart';
import 'package:nuwus_web_interface/events_list/bloc/events_list_bloc.dart';
import 'package:nuwus_web_interface/main_layout/tab_cubit.dart';
import 'package:nuwus_web_interface/repository/event_repository.dart';
import 'package:nuwus_web_interface/ressources/event.dart';
import 'package:nuwus_web_interface/widgets/snackbar.dart';
import 'package:url_launcher/url_launcher.dart';

class EventsList extends StatelessWidget {
  final bool hideHeader;
  final bool hideDividers;
  final bool hideDescription;
  final EdgeInsets padding;

  EventsList({
    Key? key,
    this.hideHeader = false,
    this.hideDividers = false,
    this.hideDescription = false,
    this.padding = const EdgeInsets.all(8),
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocListener<EventsListBloc, EventsListState>(
      listener: (BuildContext context, state) {
        if (state is EventsListHasError) {
          snackBar(context, Icons.wifi_off_rounded, 'Erreur de connexion au serveur');
          print('Erreur de connexion au serveur');
        }
      },
      child: BlocBuilder<EventsListBloc, EventsListState>(
        builder: (context, EventsListState state) {
          if (state is EventsListInitial) {
            BlocProvider.of<EventsListBloc>(context).add(ListUpdateRequested());
          }
          if (state is EventsListHasError) {
            return LayoutBuilder(
              builder: (context, constraints) {
                return RefreshIndicator(
                  color: Theme.of(context).primaryColor,
                  onRefresh: () async {
                    BlocProvider.of<EventsListBloc>(context).add(ListUpdateRequested());
                  },
                  child: Padding(
                    padding: padding,
                    child: SingleChildScrollView(
                      physics: AlwaysScrollableScrollPhysics(),
                      child: ConstrainedBox(
                        constraints: BoxConstraints(
                          minHeight: constraints.maxHeight,
                          minWidth: constraints.maxWidth,
                        ),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Text(
                                'Erreur de connexion',
                                style: Theme.of(context).textTheme.bodyText1,
                              ),
                            ),
                            OutlinedButton(
                              onPressed: () async {
                                BlocProvider.of<EventsListBloc>(context).add(ListUpdateRequested());
                              },
                              style: Theme.of(context).outlinedButtonTheme.style,
                              child: Text(
                                'Réessayer',
                                style: Theme.of(context).textTheme.bodyText1,
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                );
              },
            );
          }

          if (state is EventsListLoadSuccess) {
            return RefreshIndicator(
              color: Theme.of(context).primaryColor,
              onRefresh: () async => BlocProvider.of<EventsListBloc>(context).add(ListUpdateRequested()),
              child: Padding(
                padding: EdgeInsets.symmetric(vertical: padding.vertical),
                child: ListView.separated(
                  separatorBuilder: (BuildContext context, int i) => hideDividers
                      ? Container()
                      : Divider(
                          height: 0,
                          thickness: 1,
                          color: Theme.of(context).dividerColor,
                        ),
                  // physics: BouncingScrollPhysics(),
                  itemCount: state.eventsList.length + (hideHeader ? 0 : 2),
                  itemBuilder: (BuildContext context, int i) {
                    if (i == 0 && !hideHeader) {
                      return Padding(
                        padding: EdgeInsets.symmetric(vertical: 8, horizontal: padding.horizontal),
                        child: Text(
                          "Evénements à venir",
                          style: Theme.of(context)
                              .textTheme
                              .headline2!
                              .copyWith(color: Theme.of(context).textTheme.bodyText1!.color),
                        ),
                      );
                    } else if (i == 1 && !hideHeader) {
                      return Divider(
                        height: 0,
                        thickness: 1,
                        color: Theme.of(context).dividerColor,
                      );
                    } else {
                      if (!hideHeader) {
                        i -= 2;
                      }
                      return Padding(
                        padding: EdgeInsets.symmetric(horizontal: padding.horizontal),
                        child: GestureDetector(
                          behavior: HitTestBehavior.opaque,
                          child: EventContainer(
                            state.eventsList[i].id!,
                            state.eventsList,
                            hideDescription: hideDescription,
                          ),
                          onTap: () {
                            BlocProvider.of<EventCubit>(context).show(state.eventsList[i]);
                          },
                        ),
                      );
                    }
                  },
                ),
              ),
            );
          } else {
            return Container(
              child: Center(
                child: CircularProgressIndicator(),
              ),
            );
          }
        },
      ),
    );
  }
}

class EventContainer extends StatefulWidget {
  final List<Event> eventList;
  final int id;
  final bool hideDescription;

  const EventContainer(
    this.id,
    this.eventList, {
    Key? key,
    this.hideDescription = false,
  }) : super(key: key);

  @override
  _EventContainerState createState() => _EventContainerState();
}

class _EventContainerState extends State<EventContainer> {
  final _eventRepo = EventRepository();

  @override
  Widget build(BuildContext context) {
    var event = widget.eventList.firstWhere((element) => element.id == widget.id);
    return Row(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.only(
            top: 12,
            bottom: 12,
            right: 15,
            left: 0,
          ),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text(
                DateFormat('dd').format(event.beginTime),
                textAlign: TextAlign.center,
                style: TextStyle(
                  color: Theme.of(context).textTheme.bodyText1!.color,
                  fontSize: 40,
                  height: 0.99,
                  fontFamily: "Roboto",
                  fontWeight: FontWeight.w300,
                ),
              ),
              Text(
                DateFormat('MMM', 'fr_FR').format(event.beginTime).toUpperCase(),
                textAlign: TextAlign.center,
                style: TextStyle(
                  color: Theme.of(context).textTheme.bodyText1!.color,
                  fontSize: 14.2,
                  height: 0.8,
                  fontFamily: "Roboto",
                  fontWeight: FontWeight.normal,
                ),
              ),
            ],
          ),
        ),
        Flexible(
          child: Padding(
            padding: const EdgeInsets.only(
              top: 12,
              bottom: 12,
            ),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  event.name,
                  textAlign: TextAlign.left,
                  overflow: TextOverflow.ellipsis,
                  maxLines: 1,
                  style: TextStyle(
                    fontSize: 17,
                    fontFamily: "Roboto",
                    fontWeight: FontWeight.w600,
                  ),
                ),
                Text(
                  event.formatOrganizers(),
                  textAlign: TextAlign.left,
                  overflow: TextOverflow.ellipsis,
                  maxLines: 1,
                  style: TextStyle(
                    color: Theme.of(context).textTheme.bodyText1!.color,
                    fontSize: 15,
                    fontFamily: "Roboto",
                    fontWeight: FontWeight.normal,
                  ),
                ),
                Row(
                  children: [
                    Icon(
                      Icons.location_on,
                      color: Theme.of(context).textTheme.bodyText1!.color,
                      size: 20,
                    ),
                    Expanded(
                      child: Text(
                        event.location,
                        textAlign: TextAlign.left,
                        overflow: TextOverflow.ellipsis,
                        maxLines: 1,
                        style: TextStyle(
                          color: Theme.of(context).textTheme.bodyText1!.color,
                          fontSize: 15,
                          fontFamily: "Roboto",
                          fontWeight: FontWeight.normal,
                        ),
                      ),
                    ),
                  ],
                ),
                widget.hideDescription
                    ? Container()
                    : Padding(
                        padding: const EdgeInsets.only(top: 8),
                        child: MarkdownBody(
                          onTapLink: (text, link, title) async {
                            if (await canLaunch(link ?? '')) {
                              await launch(link ?? '');
                            } else {
                              throw 'Could not launch $link';
                            }
                          },
                          data: event.description.length >
                                  120 // todo : à améliorer pour mettre une hauteur maximale
                              ? event.description.replaceAll('\n', '\n\n').substring(0, 120) + '...'
                              : event.description.replaceAll('\n', '\n\n'),
                          styleSheet: MarkdownStyleSheet(
                            p: Theme.of(context).textTheme.bodyText1,
                            a: Theme.of(context).textTheme.bodyText1!.copyWith(color: Colors.blueAccent),
                          ),
                        ),
                        // Text(
                        //   event.description,
                        //   style: Theme.of(context).textTheme.bodyText1,
                        // ),
                      )
              ],
            ),
          ),
        ),
        Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            IconButton(
              icon: Icon(
                Icons.edit_rounded,
                color: Theme.of(context).textTheme.bodyText1!.color,
              ),
              onPressed: () {
                BlocProvider.of<EventCubit>(context).show(event);
                BlocProvider.of<TabCubit>(context).showEdit();
              },
            ),
            IconButton(
              icon: Icon(
                Icons.delete_rounded,
                color: Theme.of(context).textTheme.bodyText1!.color,
              ),
              onPressed: () {
                var loginState = BlocProvider.of<LoginBloc>(context).state;
                var listBloc = BlocProvider.of<EventsListBloc>(context);
                showDialog(
                    context: context,
                    builder: (context) {
                      return AlertDialog(
                        title: Text("Supprimer l'événement ?"),
                        content: Text("Êtes-vous sûr de vouloir supprimer l'événement ${event.name} ?"),
                        actions: [
                          OutlinedButton(
                            child: Text("Supprimer"),
                            onPressed: () async {
                              await _eventRepo.deleteEvent(
                                event.id!,
                                loginState is LoggedIn ? loginState.mail : '',
                                loginState is LoggedIn ? loginState.password : '',
                              );
                              Navigator.of(context).pop();
                              listBloc.add(ListUpdateRequested());
                            },
                          ),
                          ElevatedButton(
                              onPressed: () => Navigator.of(context).pop(), child: Text("Annuler")),
                        ],
                      );
                    });
              },
            ),
          ],
        )
      ],
    );
  }
}
