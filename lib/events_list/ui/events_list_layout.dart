import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:nuwus_web_interface/auth/bloc/login_bloc.dart';
import 'package:nuwus_web_interface/event_creator/ui/event_view.dart';
import 'package:nuwus_web_interface/events_list/bloc/events_list_bloc.dart';
import 'package:nuwus_web_interface/events_list/ui/filter_modal_sheet.dart';

import 'events_list.dart';

class ListLayoutChooser extends StatelessWidget {
  const ListLayoutChooser({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Événements'),
        actions: [
          Center(
            child: SizedBox(
              child: TextField(
                style: TextStyle(color: Theme.of(context).colorScheme.onPrimary),
                cursorColor: Theme.of(context).colorScheme.onPrimary,
                decoration: InputDecoration(
                  filled: true,
                  fillColor: Colors.white.withOpacity(0.12),
                  border: OutlineInputBorder(
                    borderRadius: BorderRadius.all(Radius.circular(8)),
                    borderSide: BorderSide.none,
                  ),
                  enabledBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.all(Radius.circular(8)),
                    borderSide: BorderSide.none,
                  ),
                  focusedBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.all(Radius.circular(8)),
                    borderSide: BorderSide.none,
                  ),
                  isDense: true,
                  contentPadding: EdgeInsets.all(0),
                  floatingLabelBehavior: FloatingLabelBehavior.never,
                  labelText: "Chercher un événement",
                  suffixIcon: IconButton(
                    icon: Icon(Icons.clear_rounded),
                    color: Theme.of(context).colorScheme.onPrimary,
                    onPressed: () {
                      BlocProvider.of<EventsListBloc>(context).add(FilterUpdateRequested(searchQuery: ''));
                      TextInputAction.done;
                    },
                  ),
                  prefixIcon: Icon(
                    Icons.search_rounded,
                    color: Theme.of(context).colorScheme.onPrimary,
                  ),
                  labelStyle: TextStyle(color: Theme.of(context).colorScheme.onPrimary),
                ),
                onChanged: (query) =>
                    BlocProvider.of<EventsListBloc>(context).add(FilterUpdateRequested(searchQuery: query)),
              ),
              width: 300,
              height: 40,
            ),
          ),
          IconButton(
              tooltip: "Options de filtrage",
              icon: Icon(Icons.filter_list_rounded),
              onPressed: () {
                showFilterPopup(context, BlocProvider.of<EventsListBloc>(context));
              }),
          IconButton(
            tooltip: "Se déconnecter",
            onPressed: () => BlocProvider.of<LoginBloc>(context).add(UserLogoutRequested()),
            icon: Icon(
              Icons.logout_rounded,
            ),
          )
        ],
      ),
      body: LayoutBuilder(builder: (context, constraints) {
        if (constraints.maxWidth > 900) {
          return ListWebLayout();
        } else {
          return ListMobileLayout();
        }
      }),
    );
  }
}

class ListWebLayout extends StatefulWidget {
  const ListWebLayout({Key? key}) : super(key: key);

  @override
  State<ListWebLayout> createState() => _ListWebLayoutState();
}

class _ListWebLayoutState extends State<ListWebLayout> {
  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Expanded(
          flex: 2,
          child: EventsList(
            hideDividers: true,
            hideHeader: true,
          ),
        ),
        Expanded(
          flex: 3,
          child: EventView(),
        ),
      ],
    );
  }
}

class ListMobileLayout extends StatelessWidget {
  ListMobileLayout({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 2,
      child: TabBarView(
        children: <Widget>[
          EventsList(
            hideDividers: true,
            hideHeader: true,
          ),
          EventView(),
        ],
      ),
    );
  }
}
