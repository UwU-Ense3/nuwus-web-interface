import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:nuwus_web_interface/events_list/bloc/events_list_bloc.dart';
import 'package:nuwus_web_interface/events_list/bloc/visibility_filter.dart';
import 'package:nuwus_web_interface/ressources/event.dart';

void showFilterPopup(BuildContext context, EventsListBloc bloc) {
  print(context);
  showModalBottomSheet<void>(
      context: context,
      isScrollControlled: true,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.only(topLeft: Radius.circular(10), topRight: Radius.circular(10)),
      ),
      builder: (BuildContext context) {
        return BlocBuilder<EventsListBloc, EventsListState>(
          bloc: bloc,
          builder: (context, state) {
            SortOrder currentSortOrder = SortOrder.chronological;
            if (state is EventsListLoadSuccess) {
              currentSortOrder = state.filter.sortOrder;
            }
            return Wrap(
              children: [
                Container(
                  decoration: BoxDecoration(
                    borderRadius:
                        BorderRadius.only(topLeft: Radius.circular(10), topRight: Radius.circular(10)),
                    color: Theme.of(context).backgroundColor,
                  ),
                  padding: EdgeInsets.all(20),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      ChoiceChip(
                        shape: StadiumBorder(side: BorderSide(color: Theme.of(context).primaryColor)),
                        selectedColor: Theme.of(context).primaryColor,
                        backgroundColor: Theme.of(context).backgroundColor,
                        label: Text(
                          'Montrer les événements passés',
                          style: Theme.of(context).textTheme.bodyText1!.copyWith(
                              color: (state is EventsListLoadSuccess ? state.filter.showPast : false)
                                  ? Colors.white
                                  : Theme.of(context).primaryColor),
                        ),
                        selected: state is EventsListLoadSuccess ? state.filter.showPast : false,
                        onSelected: (bool selected) {
                          bloc.add(FilterUpdateRequested(showPast: selected));
                        },
                      ),
                      Padding(
                        padding: const EdgeInsets.only(bottom: 12, top: 8),
                        child: Text(
                          'Trier par',
                          style: Theme.of(context).textTheme.headline2!.copyWith(
                                fontWeight: FontWeight.w700,
                              ),
                        ),
                      ),
                      buildSortTile(
                        context,
                        name: 'Date et heure',
                        currentSortOrder: currentSortOrder,
                        sortOrder: SortOrder.chronological,
                        reverseSortOrder: SortOrder.reverseChronological,
                        bloc: bloc,
                      ),
                      buildSortTile(
                        context,
                        name: 'Nom',
                        currentSortOrder: currentSortOrder,
                        sortOrder: SortOrder.alphabetical,
                        bloc: bloc,
                        reverseSortOrder: SortOrder.reverseAlphabetical,
                      ),
                      buildSortTile(
                        context,
                        name: 'Prix',
                        currentSortOrder: currentSortOrder,
                        sortOrder: SortOrder.increasingPrice,
                        bloc: bloc,
                        reverseSortOrder: SortOrder.decreasingPrice,
                      ),
                      Padding(
                        padding: const EdgeInsets.only(bottom: 12),
                        child: Text(
                          'Filtrer par tag',
                          style: Theme.of(context).textTheme.headline2!.copyWith(
                                fontWeight: FontWeight.w700,
                              ),
                        ),
                      ),
                      state is EventsListLoadSuccess
                          ? Wrap(
                              children: List.generate(
                                state.tagsList.length,
                                (i) {
                                  Tag tag = state.tagsList[i];
                                  return Padding(
                                    padding: const EdgeInsets.symmetric(horizontal: 4),
                                    child: TagChoiceChip(
                                      tag: tag,
                                      selected: state.filter.selectedTags.contains(tag),
                                      onSelected: (bool newSelected) {
                                        if (newSelected) {
                                          bloc.add(FilterUpdateRequested(
                                              selectedTags: state.filter.selectedTags..add(tag)));
                                        } else {
                                          bloc.add(FilterUpdateRequested(
                                              selectedTags: state.filter.selectedTags..remove(tag)));
                                        }
                                      },
                                    ),
                                  );
                                },
                              ),
                            )
                          : CircularProgressIndicator(),
                    ],
                  ),
                ),
              ],
            );
          },
        );
      });
}

Padding buildSortTile(BuildContext context,
    {required SortOrder currentSortOrder,
    required SortOrder sortOrder,
    required SortOrder reverseSortOrder,
    required String name,
    required EventsListBloc bloc}) {
  return Padding(
    padding: const EdgeInsets.only(bottom: 4),
    child: Container(
      decoration: BoxDecoration(
        color: currentSortOrder == sortOrder || currentSortOrder == reverseSortOrder
            ? Theme.of(context).primaryColor
            : Theme.of(context).backgroundColor,
        borderRadius: BorderRadius.all(
          Radius.circular(10),
        ),
      ),
      child: ListTile(
        trailing: Icon(
          currentSortOrder == sortOrder ? Icons.arrow_downward_rounded : Icons.arrow_upward_rounded,
          color: currentSortOrder == sortOrder || currentSortOrder == reverseSortOrder
              ? Theme.of(context).colorScheme.onPrimary
              : Colors.transparent,
        ),
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.all(
            Radius.circular(10),
          ),
        ),
        title: Text(
          name,
          style: Theme.of(context).textTheme.headline2!.copyWith(
                fontWeight: FontWeight.normal,
                color: currentSortOrder == sortOrder || currentSortOrder == reverseSortOrder
                    ? Theme.of(context).colorScheme.onPrimary
                    : Theme.of(context).textTheme.headline2!.color,
              ),
        ),
        onTap: () {
          if (currentSortOrder == sortOrder) {
            bloc.add(SortingUpdateRequested(sortOrder: reverseSortOrder));
          } else {
            bloc.add(SortingUpdateRequested(sortOrder: sortOrder));
          }
          // Navigator.pop(context);
        },
      ),
    ),
  );
}

class TagChoiceChip extends StatefulWidget {
  final Tag tag;
  final bool selected;
  final void Function(bool) onSelected;

  TagChoiceChip({Key? key, required this.tag, required this.selected, required this.onSelected})
      : super(key: key);

  @override
  _TagChoiceChipState createState() => _TagChoiceChipState();
}

class _TagChoiceChipState extends State<TagChoiceChip> {
  @override
  Widget build(BuildContext context) {
    var color = HexColor.fromHex(widget.tag.color ?? 'ffffff');
    return ChoiceChip(
      shape: StadiumBorder(side: BorderSide(color: color)),
      selectedColor: color,
      selected: widget.selected,
      onSelected: widget.onSelected,
      label: Text(
        widget.tag.name,
        style: Theme.of(context).textTheme.bodyText1!.copyWith(color: widget.selected ? Colors.white : color),
      ),
      backgroundColor: Theme.of(context).backgroundColor,
      // shadowColor:
      //     Theme.of(context).textTheme.bodyText1.color.withOpacity(0.5),
    );
  }
}
