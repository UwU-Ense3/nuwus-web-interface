import 'dart:math';

import 'package:flutter/material.dart';

/// Colors of dark and light theme
class NuwusTheme {
  // light theme colors
  static const Color _e3Blue = Color(0xFF0054A6);
  static final MaterialColor _lightPrimary = generateMaterialColor(_e3Blue);
  static const Color _lightBackground = Color(0xffffffff);
  static const Color _lightAccent = Color(0xFF69A9CA);
  static const Color _lightBorder = Color(0xFFD1D1D1);
  static const Color _lightContainer = Color(0xfff8f8f8);
  static const Color _lightText = Color(0xFF343434);

  // dark theme colors
  static const Color _uwuGreen = Color(0xff2AB528);
  static final MaterialColor _darkPrimary = generateMaterialColor(_uwuGreen);
  static const Color _darkAccent = Color(0xff2E6A2C);
  static const Color _darkBackground = Color(0xFF1d1d1d);
  static const Color _darkBorder = Color(0xFF37E833);
  static const Color _darkContainer = Color(0xff000000);
  static const Color _darkText = Color(0xFFD1D1D1);
  static const Color _darkChip = Color(0xff2E6A2C);

  // common colors
  static const Color _onPrimary = Colors.white;

  static final ThemeData lightTheme = ThemeData.light().copyWith(
      brightness: Brightness.light,
      primaryColor: _lightPrimary,
      backgroundColor: _lightBackground,
      dividerColor: _lightBorder,
      cardColor: _lightContainer,
      colorScheme: ColorScheme.light(
        primary: _lightPrimary,
        onPrimary: _onPrimary,
        secondary: _lightAccent,
        background: _lightBackground,
      ),
      appBarTheme: AppBarTheme(color: _lightPrimary),
      textTheme: TextTheme(
          bodyText1: const TextStyle(
            color: _lightText,
            fontSize: 14,
            fontFamily: "Roboto",
            fontWeight: FontWeight.normal,
          ),
          bodyText2: TextStyle(
            color: _lightText,
          ),
          headline2: const TextStyle(
            fontFamily: "Roboto",
            fontWeight: FontWeight.w600,
            color: _lightText,
            fontSize: 16,
          ),
          headline3: const TextStyle(
            fontFamily: "Roboto",
            color: _lightText,
            fontSize: 20,
            fontWeight: FontWeight.w600,
          ),
        subtitle2: TextStyle(
            fontFamily: "Roboto",
            fontWeight: FontWeight.w300,
            color: _lightPrimary,
            fontSize: 16,
          ),
          caption: const TextStyle(
            color: _lightText,
            fontSize: 14,
            fontFamily: "Roboto",
            fontWeight: FontWeight.w600,
          ),
          subtitle1: const TextStyle(
            color: _lightText,
            fontSize: 14,
            fontFamily: "Roboto",
            fontWeight: FontWeight.normal,
          )),
      outlinedButtonTheme: OutlinedButtonThemeData(
        style: OutlinedButton.styleFrom(
          primary: _lightPrimary,
          shape: RoundedRectangleBorder(borderRadius: const BorderRadius.all(Radius.circular(4))),
          side: BorderSide(
            color: _lightPrimary.withOpacity(0.12),
            width: 1,
          ),
          textStyle: TextStyle(
            fontFamily: "Roboto",
            color: _lightPrimary,
          ),
        ),
      ),
      elevatedButtonTheme: ElevatedButtonThemeData(
        style: ElevatedButton.styleFrom(
          primary: _lightPrimary,
          textStyle: const TextStyle(
            fontFamily: "Roboto",
            color: _onPrimary,
          ),
        ),
      ),
      inputDecorationTheme: InputDecorationTheme(
        labelStyle: const TextStyle(
          color: _lightText,
          fontSize: 14,
          fontFamily: "Roboto",
          fontWeight: FontWeight.normal,
        ),
        filled: true,
        fillColor: Color(0x5000000),
        errorBorder: OutlineInputBorder(
            borderSide: BorderSide(
              color: Color(0xcdc11f1f),
              width: 1.8,
              style: BorderStyle.solid,
            ),
            borderRadius: BorderRadius.all(Radius.circular(5.0)),
            gapPadding: 4),
        focusedBorder: OutlineInputBorder(
            borderSide: BorderSide(
              color: _lightPrimary,
              width: 2,
              style: BorderStyle.solid,
            ),
            borderRadius: BorderRadius.all(Radius.circular(5.0)),
            gapPadding: 4),
        focusedErrorBorder: OutlineInputBorder(
            borderSide: BorderSide(
              color: Color(0xcdc11f1f),
              width: 2,
              style: BorderStyle.solid,
            ),
            borderRadius: BorderRadius.all(Radius.circular(5.0)),
            gapPadding: 4),
        disabledBorder: OutlineInputBorder(
            borderSide: BorderSide(
              color: Color(0x372D2D2D),
              width: 1.8,
              style: BorderStyle.solid,
            ),
            borderRadius: BorderRadius.all(Radius.circular(5.0)),
            gapPadding: 4),
        enabledBorder: OutlineInputBorder(
            borderSide: BorderSide(
              color: Color(0x812D2D2D),
              width: 1.8,
              style: BorderStyle.solid,
            ),
            borderRadius: BorderRadius.all(Radius.circular(5.0)),
            gapPadding: 4),
      ));

  static final ThemeData darkTheme = ThemeData.dark().copyWith(
    brightness: Brightness.dark,
    primaryColor: _darkPrimary,
    backgroundColor: _darkBackground,
    scaffoldBackgroundColor: _darkBackground,
    dialogBackgroundColor: _darkBackground,
    canvasColor: _darkBackground,
    dividerColor: _darkBorder,
    cardColor: _darkContainer,
    chipTheme: ChipThemeData.fromDefaults(
      secondaryColor: _darkChip,
      brightness: Brightness.dark,
      labelStyle: TextStyle(
        color: _darkText,
      ),
    ).copyWith(selectedColor: _darkChip),
    colorScheme: ColorScheme.dark(
      primary: _darkPrimary,
      onPrimary: _onPrimary,
      secondary: _darkAccent,
      background: _darkBackground,
    ),
    appBarTheme: AppBarTheme(color: _darkPrimary),
    textTheme: TextTheme(
      bodyText1: TextStyle(
        color: _darkText,
        fontSize: 14,
        fontFamily: "Roboto",
        fontWeight: FontWeight.normal,
      ),
      bodyText2: TextStyle(
        color: _darkText,
      ),
      headline2: TextStyle(
        fontFamily: "Roboto",
        fontWeight: FontWeight.w600,
        color: _darkText,
        fontSize: 16,
      ),
      headline3: TextStyle(
        fontFamily: "Roboto",
        color: _darkText,
        fontSize: 20,
      ),
      subtitle2: TextStyle(
        fontFamily: "Roboto",
        fontWeight: FontWeight.w100,
        color: _darkPrimary,
        fontSize: 14,
      ),
    ),
    outlinedButtonTheme: OutlinedButtonThemeData(
      style: OutlinedButton.styleFrom(
        primary: _darkPrimary,
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(4))),
        side: BorderSide(
          color: _darkPrimary.withOpacity(0.12),
          width: 1,
        ),
        textStyle: TextStyle(
          color: _darkPrimary,
        ),
      ),
    ),
    elevatedButtonTheme: ElevatedButtonThemeData(
      style: ElevatedButton.styleFrom(
        primary: _darkPrimary,
        textStyle: TextStyle(
          fontFamily: "Roboto",
          color: _onPrimary,
        ),
      ),
    ),
  );
}

MaterialColor generateMaterialColor(Color color) {
  return MaterialColor(color.value, {
    50: tintColor(color, 0.9),
    100: tintColor(color, 0.8),
    200: tintColor(color, 0.6),
    300: tintColor(color, 0.4),
    400: tintColor(color, 0.2),
    500: color,
    600: shadeColor(color, 0.1),
    700: shadeColor(color, 0.2),
    800: shadeColor(color, 0.3),
    900: shadeColor(color, 0.4),
  });
}

int tintValue(int value, double factor) => max(0, min((value + ((255 - value) * factor)).round(), 255));

Color tintColor(Color color, double factor) => Color.fromRGBO(
    tintValue(color.red, factor), tintValue(color.green, factor), tintValue(color.blue, factor), 1);

int shadeValue(int value, double factor) => max(0, min(value - (value * factor).round(), 255));

Color shadeColor(Color color, double factor) => Color.fromRGBO(
    shadeValue(color.red, factor), shadeValue(color.green, factor), shadeValue(color.blue, factor), 1);
