import 'package:bloc/bloc.dart';
import 'package:nuwus_web_interface/repository/event_repository.dart';
import 'package:nuwus_web_interface/ressources/event.dart';

class EventCubit extends Cubit<Event> {
  EventCubit(Event event) : super(event);
  final eventRepository = EventRepository();

  void update(void Function(Event e) updater) {
    var newEvent = state.copy();
    updater(newEvent);
    emit(newEvent);
  }

  void show(Event event) {
    emit(event);
  }

  Future<void> fetchAndShowById(int id) async {
    emit(await eventRepository.fetchEvent(id));
  }

  void resetToDefault() {
    emit(Event.defaultEvent);
  }
}
