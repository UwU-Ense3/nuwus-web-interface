import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:nuwus_web_interface/auth/bloc/login_bloc.dart';

import 'event_form.dart';
import 'event_view.dart';

class FormLayoutChooser extends StatelessWidget {
  final bool editMode;

  const FormLayoutChooser({Key? key, this.editMode = false}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Ajouter un événement'),
        actions: [
          IconButton(
            tooltip: "Se déconnecter",
            onPressed: () => BlocProvider.of<LoginBloc>(context).add(UserLogoutRequested()),
            icon: Icon(
              Icons.logout_rounded,
            ),
          )
        ],
      ),
      body: LayoutBuilder(builder: (context, constraints) {
        if (constraints.maxWidth > 900) {
          return WebLayout(editMode);
        } else {
          return MobileLayout(editMode);
        }
      }),
    );
  }
}

class WebLayout extends StatefulWidget {
  final bool editMode;

  const WebLayout(this.editMode, {Key? key}) : super(key: key);

  @override
  State<WebLayout> createState() => _WebLayoutState();
}

class _WebLayoutState extends State<WebLayout> {
  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Expanded(
          flex: 2,
          child: EventForm(editMode: widget.editMode),
        ),
        Expanded(
          flex: 3,
          child: EventView(),
        ),
      ],
    );
  }
}

class MobileLayout extends StatelessWidget {
  final bool editMode;

  MobileLayout(this.editMode, {Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 2,
      child: TabBarView(
        children: <Widget>[
          EventForm(editMode: editMode),
          EventView(),
        ],
      ),
    );
  }
}
