import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_markdown/flutter_markdown.dart';
import 'package:intl/intl.dart';
import 'package:nuwus_web_interface/event_creator/cubit/event_cubit.dart';
import 'package:nuwus_web_interface/ressources/event.dart';
import 'package:url_launcher/url_launcher.dart';

class EventView extends StatelessWidget {
  final Event? event;

  EventView({key, this.event}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<EventCubit, Event>(
      builder: (context, event) => SafeArea(
        child: Stack(
          clipBehavior: Clip.none,
          children: [
            Container(
              height: 250,
              width: MediaQuery.of(context).size.width,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.vertical(bottom: Radius.circular(4)),
                boxShadow: [
                  BoxShadow(
                    offset: Offset(1, 1),
                    blurRadius: 5,
                    color: Colors.black.withOpacity(0.25),
                  ),
                ],
                color: Theme.of(context).colorScheme.secondary,
              ),
              child: ClipRRect(
                borderRadius: BorderRadius.vertical(bottom: Radius.circular(4)),
                child: Hero(
                  tag: 'imageHero',
                  child: Image.network(
                    event.imageLink,
                    fit: BoxFit.cover,
                    errorBuilder: (context, object, stackTrace) => Icon(
                      Icons.image_not_supported_rounded,
                      color: Theme.of(context).colorScheme.onPrimary.withOpacity(0.2),
                      size: 150,
                    ),
                  ),
                ),
              ),
            ),
            ListView(
              controller: ScrollController(),
              children: [
                GestureDetector(
                  onTap: () => Navigator.push(context, MaterialPageRoute(builder: (_) {
                    return ImageView(url: event.imageLink);
                  })),
                  child: Container(
                    height: 180,
                    width: MediaQuery.of(context).size.width,
                    color: Colors.transparent,
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(
                    left: 12.0,
                    right: 12,
                    top: 0,
                    bottom: 12,
                  ),
                  child: Container(
                    decoration: BoxDecoration(
                      color: Theme.of(context).cardColor,
                      border: Border.all(
                        color: Theme.of(context).dividerColor,
                        width: 1,
                      ),
                      borderRadius: BorderRadius.circular(4),
                    ),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Padding(
                          padding: const EdgeInsets.only(top: 16, right: 20, left: 20, bottom: 0),
                          child: Text(
                            event.formatFullDatetime(full: true),
                            style: Theme.of(context).textTheme.subtitle2,
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(top: 7, right: 20, left: 20, bottom: 0),
                          child: Text(
                            event.name,
                            style: Theme.of(context).textTheme.headline3,
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(top: 3, right: 20, left: 20, bottom: 0),
                          child: Row(
                            children: [
                              Icon(
                                Icons.location_on,
                                color: Colors.black87,
                                size: 20,
                              ),
                              Text(
                                event.location,
                                style: Theme.of(context).textTheme.bodyText1,
                              ),
                            ],
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(top: 20, right: 20, left: 20, bottom: 0),
                          child: Container(
                            height: 35,
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceAround,
                              children: [
                                Row(
                                  children: [
                                    Padding(
                                      padding: const EdgeInsets.symmetric(horizontal: 5.0),
                                      child: Icon(
                                        Icons.attach_money_rounded,
                                        size: 25,
                                      ),
                                    ),
                                    Padding(
                                      padding: const EdgeInsets.symmetric(horizontal: 5.0),
                                      child: Text(
                                        event.price == 0 ? '-' : event.price.toString() + ' €',
                                        style: Theme.of(context).textTheme.bodyText2,
                                      ),
                                    ),
                                  ],
                                ),
                                VerticalDivider(
                                  width: 0,
                                  color: Theme.of(context).dividerColor,
                                  thickness: 1,
                                ),
                                Row(
                                  children: [
                                    Padding(
                                      padding: const EdgeInsets.symmetric(horizontal: 5.0),
                                      child: Icon(
                                        Icons.people_rounded,
                                        size: 25,
                                      ),
                                    ),
                                    Padding(
                                      padding: const EdgeInsets.symmetric(horizontal: 5.0),
                                      child: Text(
                                        event.numberParticipants == 0
                                            ? '-'
                                            : event.numberParticipants.toString(),
                                        style: Theme.of(context).textTheme.bodyText2,
                                      ),
                                    ),
                                  ],
                                ),
                                VerticalDivider(
                                  width: 0,
                                  color: Theme.of(context).dividerColor,
                                  thickness: 1,
                                ),
                                Row(
                                  children: [
                                    Padding(
                                      padding: const EdgeInsets.symmetric(horizontal: 5.0),
                                      child: Icon(
                                        Icons.calendar_today,
                                        size: 25,
                                      ),
                                    ),
                                    Padding(
                                      padding: const EdgeInsets.symmetric(horizontal: 5.0),
                                      child: Text(
                                        DateFormat('dd/MM').format(event.beginTime),
                                        style: Theme.of(context).textTheme.bodyText2,
                                      ),
                                    ),
                                  ],
                                ),
                              ],
                            ),
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(left: 20, right: 20, top: 15, bottom: 0),
                          child: Text(
                            'Organisé par ' + event.formatOrganizers(),
                            overflow: TextOverflow.fade,
                            style: Theme.of(context).textTheme.caption,
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(left: 20, right: 20, top: 23, bottom: 0),
                          child: MarkdownBody(
                            onTapLink: (text, link, title) async {
                              if (await canLaunch(link ?? '')) {
                                await launch(link ?? '');
                              } else {
                                throw 'Could not launch $link';
                              }
                            },
                            data: event.description.replaceAll('\n', '\n\n'),
                            styleSheet: MarkdownStyleSheet(
                              p: Theme.of(context).textTheme.bodyText1,
                              a: Theme.of(context).textTheme.bodyText1,
                            ),
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(left: 20, right: 20, top: 20, bottom: 3),
                          child: Text(
                            'Tags : ',
                            style: Theme.of(context).textTheme.bodyText1,
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(left: 20, right: 20, top: 0, bottom: 20),
                          child: Container(
                            height: 40,
                            child: ListView.builder(
                              scrollDirection: Axis.horizontal,
                              itemCount: event.tags.length,
                              itemBuilder: (context, int i) => Padding(
                                padding: const EdgeInsets.only(right: 8),
                                child: Chip(
                                  labelPadding: EdgeInsets.all(2.0),
                                  label:
                                      Text(event.tags[i].name, style: Theme.of(context).textTheme.bodyText1),
                                  backgroundColor: Theme.of(context).backgroundColor,
                                  elevation: 3.0,
                                  // shadowColor:
                                  //     Theme.of(context).textTheme.bodyText1.color.withOpacity(0.5),
                                  padding: EdgeInsets.all(8.0),
                                ),
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}

/// Vue de l'image d'illustration en plein écran
class ImageView extends StatelessWidget {
  final String url;

  const ImageView({Key? key, this.url = ''}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      child: Scaffold(
        backgroundColor: Colors.black,
        body: Center(
          child: Hero(
            tag: 'imageHero',
            child: Image.network(
              url,
              fit: BoxFit.cover,
              errorBuilder: (context, object, stackTrace) => Icon(
                Icons.image_not_supported_rounded,
                color: Theme.of(context).colorScheme.onPrimary.withOpacity(0.2),
                size: 150,
              ),
            ),
          ),
        ),
      ),
      onTap: () {
        Navigator.pop(context);
      },
    );
  }
}
