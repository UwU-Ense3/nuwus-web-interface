import 'package:date_time_picker/date_time_picker.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_dropzone/flutter_dropzone.dart';
import 'package:nuwus_web_interface/auth/bloc/login_bloc.dart';
import 'package:nuwus_web_interface/event_creator/cubit/event_cubit.dart';
import 'package:nuwus_web_interface/events_list/bloc/events_list_bloc.dart';
import 'package:nuwus_web_interface/imgur/imgur_repository.dart';
import 'package:nuwus_web_interface/networking/api_helper.dart';
import 'package:nuwus_web_interface/repository/event_repository.dart';
import 'package:nuwus_web_interface/ressources/event.dart';
import 'package:nuwus_web_interface/widgets/snackbar.dart';

class EventForm extends StatelessWidget {
  final _eventRepo = EventRepository();
  final bool editMode;
  final _imgurClient = ImgurRepository();

  EventForm({Key? key, this.editMode = false}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    // Controllers :
    var idController = TextEditingController();
    var nameController = TextEditingController();
    var beginTimeController = TextEditingController();
    var endTimeController = TextEditingController();
    var locationController = TextEditingController();
    var imageLinkController = TextEditingController();
    var participantsController = TextEditingController(text: '0');
    var priceController = TextEditingController(text: '0');
    var descriptionController = TextEditingController();
    var tagsController = TextEditingController();
    var organizersController = TextEditingController();
    late DropzoneViewController dropzoneController;

    // function to clear all text fields
    void clear() {
      BlocProvider.of<EventCubit>(context).resetToDefault();
    }

    void updateTextFields(BuildContext context) {
      final event = BlocProvider.of<EventCubit>(context).state;

      if (idController.text != event.id.toString()) {
        idController.text = event.id.toString();
      }
      if (nameController.text != event.name) {
        nameController.text = event.name;
      }
      if (beginTimeController.text != event.beginTime.toString()) {
        beginTimeController.text = event.beginTime.toString();
      }
      if (endTimeController.text != event.endTime.toString()) {
        endTimeController.text = event.endTime.toString();
      }
      if (locationController.text != event.location) {
        locationController.text = event.location;
      }
      if (imageLinkController.text != event.imageLink) {
        imageLinkController.text = event.imageLink;
      }
      if (event.numberParticipants !=
              int.parse(participantsController.text == '' ? '0' : participantsController.text) &&
          participantsController.text != '') {
        participantsController.text = event.numberParticipants.toString();
      }
      if (int.parse(priceController.text == '' ? '0' : priceController.text) != event.price &&
          priceController.text != '') {
        priceController.text = event.price.toString();
      }
      if (descriptionController.text != event.description) {
        descriptionController.text = event.description;
      }
      if (formatList(tagsController.text, (name) => Tag(null, name, null)).join(',') !=
          event.tags.join(',')) {
        tagsController.text = event.tags.join(',');
      }
      if (formatList(organizersController.text, (name) => Organizer(null, name)).join(',') !=
          event.organizers.join(',')) {
        organizersController.text = event.formatOrganizers();
      }
    }

    return BlocBuilder<EventCubit, Event>(builder: (context, event) {
      updateTextFields(context);
      return Scaffold(
        floatingActionButton: FloatingActionButton(
          child: const Icon(Icons.send_rounded),
          onPressed: () async {
            var loginState = BlocProvider.of<LoginBloc>(context).state;
            try {
              Event newEvent;
              if (!editMode) {
                newEvent = await _eventRepo.createEvent(
                  event,
                  loginState is LoggedIn ? loginState.mail : '',
                  loginState is LoggedIn ? loginState.password : '',
                );
                BlocProvider.of<EventsListBloc>(context).add(ListUpdateRequested());
              } else {
                newEvent = await _eventRepo.editEvent(
                  event.id!,
                  event,
                  loginState is LoggedIn ? loginState.mail : '',
                  loginState is LoggedIn ? loginState.password : '',
                );
                BlocProvider.of<EventsListBloc>(context).add(ListUpdateRequested());
              }
              snackBar(context, Icons.auto_awesome,
                  'L\'événement ${newEvent.name} a bien été ${editMode ? "modifié" : "ajouté"}');
              if (!editMode) {
                clear();
              }
            } on HttpResponseException catch (e) {
              snackBar(context, Icons.error_rounded,
                  'Erreur ${e.statusCode} durant la ${editMode ? "modification" : "création"} de l\'événement');
              print(e);
            } catch (e) {
              snackBar(context, Icons.error_rounded,
                  'Erreur durant la ${editMode ? "modification" : "création"} de l\'événement');
              print(e);
            }
          },
        ),
        body: ListView(
          controller: ScrollController(),
          children: [
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 8),
              child: Builder(
                builder: (context) {
                  if (!editMode) {
                    return Row(
                      children: [
                        OutlinedButton(
                          child: Text("Rétablir les valeurs par défaut"),
                          onPressed: clear,
                        ),
                      ],
                    );
                  } else {
                    return Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Padding(
                          padding: const EdgeInsets.only(right: 8.0),
                          child: SizedBox(
                            width: 75,
                            child: TextField(
                              controller: idController,
                              decoration: const InputDecoration(
                                label: Text(
                                  'Id',
                                ),
                              ),
                              inputFormatters: [
                                FilteringTextInputFormatter.allow(RegExp(r'[0-9]')),
                              ],
                              onChanged: (String? text) => BlocProvider.of<EventCubit>(context)
                                  .update((event) => event.id = (text == null ? null : int.parse(text))),
                            ),
                          ),
                        ),
                        OutlinedButton(
                          onPressed: () async {
                            final id = event.id;
                            if (id != null) {
                              try {
                                await BlocProvider.of<EventCubit>(context).fetchAndShowById(id);
                              } on HttpResponseException catch (e) {
                                if (e.statusCode == 404) {
                                  snackBar(context, Icons.error_rounded,
                                      'L\'événement ${event.id} n\'existe pas dans la base de donnée');
                                } else {
                                  snackBar(context, Icons.error_rounded, 'Erreur serveur : ${e.statusCode}');
                                }
                                print(e);
                              } catch (e) {
                                snackBar(context, Icons.error_rounded, 'Erreur inconnue');
                                print(e);
                              }
                            } else {
                              snackBar(context, Icons.error_rounded, 'Entrez un id');
                            }
                          },
                          child: Text('Préremplir les champs'),
                        )
                      ],
                    );
                  }
                },
              ),
            ),
            // Padding(
            //   padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 8),
            //   child: Container(
            //     decoration: const BoxDecoration(
            //       color: Color(0x05000000),
            //       borderRadius: BorderRadius.all(Radius.circular(5.0)),
            //       border: Border.fromBorderSide(
            //         BorderSide(
            //           color: Color(0x812D2D2D),
            //           width: 1.8,
            //           style: BorderStyle.solid,
            //         ),
            //       ),
            //     ),
            //     child: DropdownButton<String>(
            //       alignment: Alignment.centerLeft,
            //       borderRadius: const BorderRadius.all(Radius.circular(5.0)),
            //       underline: Container(),
            //       value: event.eventType.name,
            //       onChanged: (value) {
            //         BlocProvider.of<EventCubit>(context)
            //             .update((event) => event.eventType.name = value ?? '');
            //       },
            //       items: <String>[
            //         "Soirée",
            //         "Repas",
            //         "Conférence",
            //         "Sport",
            //         "Réunion/Présentation",
            //         "Autre",
            //       ].map<DropdownMenuItem<String>>((String value) {
            //         return DropdownMenuItem<String>(
            //           value: value,
            //           child: Text(
            //             value,
            //           ),
            //         );
            //       }).toList(),
            //     ),
            //   ),
            // ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 8),
              child: TextField(
                controller: nameController,
                decoration: const InputDecoration(
                  label: Text(
                    'Nom',
                  ),
                ),
                onChanged: (String? text) =>
                    BlocProvider.of<EventCubit>(context).update((event) => event.name = text ?? ''),
              ),
            ),
            Padding(
                padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 8),
                child: DateTimePicker(
                  controller: beginTimeController,
                  use24HourFormat: true,
                  locale: const Locale("fr", "FR"),
                  calendarTitle: "Date de début",
                  decoration: InputDecoration(
                    icon: const Icon(Icons.event),
                    label: Text(
                      'Date et heure de début de l\'événement',
                      style: Theme.of(context).textTheme.bodyText1,
                    ),
                  ),
                  firstDate: DateTime(2000),
                  lastDate: DateTime(2100),
                  type: DateTimePickerType.dateTime,
                  onChanged: (val) {
                    BlocProvider.of<EventCubit>(context).update((event) {
                      event.beginTime = DateTime.parse(val);
                      if (event.endTime.isBefore(event.beginTime)) {
                        event.endTime = event.beginTime.add(Duration(hours: 1));
                      }
                    });
                  },
                )),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 8),
              child: DateTimePicker(
                use24HourFormat: true,
                autovalidate: true,
                calendarTitle: "Date de fin",
                locale: const Locale("fr", "FR"),
                selectableDayPredicate: (endDateTime) {
                  return endDateTime.day >= event.beginTime.day;
                },
                validator: (value) {
                  if (DateTime.parse(value ?? '').isBefore(event.beginTime)) {
                    return 'Vérifiez les horaires de l\'événement';
                  } else {
                    return null;
                  }
                },
                controller: endTimeController,
                decoration: InputDecoration(
                  icon: const Icon(Icons.event),
                  label: Text(
                    'Date et heure de fin de l\'événement',
                    style: Theme.of(context).textTheme.bodyText1,
                  ),
                ),
                firstDate: DateTime(2000),
                lastDate: DateTime(2100),
                type: DateTimePickerType.dateTime,
                onChanged: (val) => BlocProvider.of<EventCubit>(context)
                    .update((event) => event.endTime = DateTime.parse(val)),
              ),
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 8),
              child: TextField(
                controller: organizersController,
                decoration: InputDecoration(
                  label: Text(
                    'Organisateurs',
                    style: Theme.of(context).textTheme.bodyText1,
                  ),
                ),
                onChanged: (String? text) => BlocProvider.of<EventCubit>(context).update(
                  (event) => event.organizers = formatList<Organizer>(text, (name) => Organizer(null, name)),
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 8),
              child: TextField(
                controller: locationController,
                decoration: InputDecoration(
                  label: Text(
                    'Lieu',
                    style: Theme.of(context).textTheme.bodyText1,
                  ),
                ),
                onChanged: (String? text) =>
                    BlocProvider.of<EventCubit>(context).update((event) => event.location = text ?? ''),
              ),
            ),
            SizedBox(
              height: 65,
              child: Stack(
                children: [
                  kIsWeb
                      ? DropzoneView(
                          operation: DragOperation.copy,
                          onCreated: (DropzoneViewController ctrl) => dropzoneController = ctrl,
                          // onLoaded: () => print('Zone loaded'),
                          // onError: (String? ev) => print('Error: $ev'),
                          // onHover: () => print('Zone hovered'),
                          onDrop: (dynamic file) async {
                            imageLinkController.text = 'Chargement...';
                            try {
                              var link = await _imgurClient.uploadImage(file);
                              BlocProvider.of<EventCubit>(context).update((event) => event.imageLink = link);
                            } on Exception catch (e) {
                              imageLinkController.text = 'Impossible d\'uploader ce fichier';
                            }
                          },
                          // onLeave: () => print('Zone left'),
                        )
                      : Container(),
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 8),
                    child: TextField(
                      controller: imageLinkController,
                      decoration: InputDecoration(
                        suffixIcon: kIsWeb
                            ? IconButton(
                                tooltip: "Uploader une image",
                                icon: Icon(Icons.file_upload_rounded),
                                onPressed: () async {
                                  var files = await dropzoneController.pickFiles();
                                  imageLinkController.text = 'Chargement...';
                                  try {
                                    var link = await _imgurClient.uploadImage(files[0]);
                                    BlocProvider.of<EventCubit>(context)
                                        .update((event) => event.imageLink = link);
                                  } on Exception catch (e) {
                                    imageLinkController.text = 'Impossible d\'uploader ce fichier';
                                  }
                                },
                              )
                            : null,
                        label: Text(
                          kIsWeb
                              ? 'Lien vers l\'image d\'illustration (glissez-déposez une image)'
                              : 'Lien vers l\'image d\'illustration',
                          style: Theme.of(context).textTheme.bodyText1,
                        ),
                      ),
                      onChanged: (String? text) => BlocProvider.of<EventCubit>(context)
                          .update((event) => event.imageLink = text ?? ''),
                    ),
                  ),
                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 8),
              child: TextField(
                controller: descriptionController,
                maxLines: null,
                decoration: InputDecoration(
                  label: Text(
                    'Description',
                    style: Theme.of(context).textTheme.bodyText1,
                  ),
                ),
                onChanged: (String? text) =>
                    BlocProvider.of<EventCubit>(context).update((event) => event.description = text ?? ''),
              ),
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 8),
              child: TextField(
                controller: priceController,
                keyboardType: TextInputType.number,
                decoration: InputDecoration(
                  label: Text(
                    'Prix',
                    style: Theme.of(context).textTheme.bodyText1,
                  ),
                ),
                inputFormatters: [
                  FilteringTextInputFormatter.allow(RegExp(r'[0-9]')),
                ],
                onChanged: (String? text) => BlocProvider.of<EventCubit>(context)
                    .update((event) => event.price = int.parse(text == '' || text == null ? '0' : text)),
              ),
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 8),
              child: TextField(
                controller: participantsController,
                keyboardType: TextInputType.number,
                decoration: InputDecoration(
                  label: Text(
                    'Nombre de participants',
                    style: Theme.of(context).textTheme.bodyText1,
                  ),
                ),
                inputFormatters: [
                  FilteringTextInputFormatter.allow(RegExp(r'[0-9]')),
                ],
                onChanged: (String? text) => BlocProvider.of<EventCubit>(context).update(
                    (event) => event.numberParticipants = int.parse(text == '' || text == null ? '0' : text)),
              ),
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 8),
              child: TextField(
                controller: tagsController,
                decoration: InputDecoration(
                  label: Text(
                    'Tags',
                    style: Theme.of(context).textTheme.bodyText1,
                  ),
                ),
                onChanged: (String? text) => BlocProvider.of<EventCubit>(context).update(
                  (event) => event.tags = formatList<Tag>(text, (name) => Tag(null, name, null)),
                ),
              ),
            ),
          ],
        ),
      );
    });
  }

  List<T> formatList<T>(String? text, T Function(String) constructor) {
    return ((text ?? '').split(',').where((element) {
      return element.trim() != '';
    }).map((element) {
      return constructor(element.trim());
    }).toList());
  }
}
