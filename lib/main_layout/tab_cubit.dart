import 'package:bloc/bloc.dart';

class TabCubit extends Cubit<int> {
  TabCubit() : super(0);

  void showForm() {
    emit(0);
  }

  void showCreate() {
    emit(1);
  }

  void showEdit() {
    emit(2);
  }

  void showTabWithIndex(int i) {
    emit(i);
  }
}
