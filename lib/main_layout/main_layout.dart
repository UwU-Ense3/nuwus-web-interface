import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:nuwus_web_interface/event_creator/ui/form_layout.dart';
import 'package:nuwus_web_interface/events_list/ui/events_list_layout.dart';
import 'package:nuwus_web_interface/main_layout/tab_cubit.dart';

class MainLayoutChooser extends StatelessWidget {
  const MainLayoutChooser({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(
      builder: (context, constraints) {
        return BlocBuilder<TabCubit, int>(
          builder: (context, index) {
            // ecran large :
            if (constraints.maxWidth > 900) {
              return Row(
                children: [
                  NavigationRail(
                    destinations: [
                      NavigationRailDestination(icon: Icon(Icons.view_list_rounded), label: Text("Liste")),
                      NavigationRailDestination(icon: Icon(Icons.add_rounded), label: Text("Créer")),
                      NavigationRailDestination(icon: Icon(Icons.edit_rounded), label: Text("Modifier")),
                    ],
                    labelType: NavigationRailLabelType.selected,
                    selectedIndex: index,
                    onDestinationSelected: (int newIndex) =>
                        BlocProvider.of<TabCubit>(context).showTabWithIndex(newIndex),
                  ),
                  VerticalDivider(
                    width: 1,
                    thickness: 1,
                    color: Colors.grey[300],
                  ),
                  Expanded(
                    child: Builder(builder: (context) {
                      if (index == 0) {
                        return ListLayoutChooser();
                      }
                      if (index == 1) {
                        return FormLayoutChooser();
                      } else {
                        return FormLayoutChooser(editMode: true);
                      }
                    }),
                  ),
                ],
              );
            } else {
              return Material(
                child: DefaultTabController(
                  length: 3,
                  child: Column(
                    children: [
                      Expanded(
                        child: Builder(builder: (context) {
                          if (index == 0) {
                            return ListLayoutChooser();
                          }
                          if (index == 1) {
                            return FormLayoutChooser();
                          } else {
                            return FormLayoutChooser(editMode: true);
                          }
                        }),
                      ),
                      TabBar(
                        labelColor: Theme.of(context).textTheme.bodyText1!.color,
                        onTap: (int newIndex) =>
                            BlocProvider.of<TabCubit>(context).showTabWithIndex(newIndex),
                        tabs: [
                          Tab(
                            icon: Icon(
                              Icons.view_list_rounded,
                            ),
                            child: Text("Liste"),
                          ),
                          Tab(
                            icon: Icon(Icons.add_rounded),
                            child: Text("Créer"),
                          ),
                          Tab(
                            icon: Icon(Icons.edit_rounded),
                            child: Text("Modifier"),
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
              );
            }
          },
        );
      },
    );
  }
}
