import 'dart:convert';

import 'package:nuwus_web_interface/networking/api_helper.dart';
import 'package:nuwus_web_interface/ressources/event.dart';

class EventRepository {
  final String url = 'api/event';
  final ApiHelper _api = ApiHelper();

  Future<List<Event>> fetchEventsList({hidePast = true}) async {
    final response = await _api.get(url, headers: {
      'hide_past': hidePast.toString(),
    });
    // print(response);
    var eventsList = <Event>[];
    response.forEach((v) {
      eventsList.add(Event.fromJson(v));
    });
    return eventsList;
  }

  Future<Event> fetchEvent(int id) async {
    final response = await _api.get(url + '/' + id.toString());
    return Event.fromJson(response);
  }

  Future<Event> createEvent(Event event, String mail, String password) async {
    final response = await _api.post(url, json.encode(event), headers: {
      'mail': mail,
      'password': password,
    });
    return Event.fromJson(response);
  }

  Future<Event> editEvent(int id, Event event, String mail, String password) async {
    final response = await _api.put(url + '/' + id.toString(), json.encode(event), headers: {
      'mail': mail,
      'password': password,
    });
    return Event.fromJson(response);
  }

  Future<void> deleteEvent(int id, String mail, String password) async {
    await _api.delete(url + '/' + id.toString(), headers: {
      'mail': mail,
      'password': password,
    });
  }
}

class OrganizerRepository {
  final String url = 'api/organizer';
  final ApiHelper _api = ApiHelper();

  Future<List<Organizer>> fetchOrganizersList() async {
    final response = await _api.get(url);
    var organizersList = <Organizer>[];
    response.forEach((v) {
      organizersList.add(Organizer.fromJson(v));
    });
    return organizersList;
  }

  Future<Organizer> fetchOrganizer(int id) async {
    final response = await _api.get(url + '/' + id.toString());
    return Organizer.fromJson(response);
  }
}

class TagRepository {
  final String url = 'api/tag';
  final ApiHelper _api = ApiHelper();

  Future<List<Tag>> fetchTagsList() async {
    final response = await _api.get(url);
    var tagsList = <Tag>[];
    response.forEach((v) {
      tagsList.add(Tag.fromJson(v));
    });
    return tagsList;
  }

  Future<Tag> fetchTag(int id) async {
    final response = await _api.get(url + '/' + id.toString());
    return Tag.fromJson(response);
  }
}
