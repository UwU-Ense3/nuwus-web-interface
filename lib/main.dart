import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:intl/date_symbol_data_local.dart';
import 'package:nuwus_web_interface/events_list/bloc/events_list_bloc.dart';
import 'package:nuwus_web_interface/main_layout/tab_cubit.dart';
import 'package:nuwus_web_interface/ressources/event.dart';
import 'package:nuwus_web_interface/theme.dart';

import 'auth/bloc/login_bloc.dart';
import 'auth/ui/login_layout.dart';
import 'event_creator/cubit/event_cubit.dart';
import 'main_layout/main_layout.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      scrollBehavior: MaterialScrollBehavior().copyWith(
        dragDevices: {PointerDeviceKind.mouse, PointerDeviceKind.touch},
      ),
      localizationsDelegates: [GlobalMaterialLocalizations.delegate],
      supportedLocales: [const Locale('en'), const Locale('fr')],
      title: 'NUwUs web interface',
      theme: NuwusTheme.lightTheme,
      home: const Home(title: 'NUwUs web interface'),
    );
  }
}

class Home extends StatefulWidget {
  const Home({Key? key, required this.title}) : super(key: key);
  final String title;

  @override
  State<Home> createState() => _HomeState();
}

class _HomeState extends State<Home> {
  @override
  void initState() {
    super.initState();
    initializeDateFormatting();
  }

  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider(create: (context) => EventCubit(Event.defaultEvent)),
        BlocProvider(create: (context) => LoginBloc()),
        BlocProvider(create: (context) => EventsListBloc()),
        BlocProvider(create: (context) => TabCubit()),
      ],
      child: DefaultTextStyle(
        style: Theme.of(context).textTheme.bodyText1!,
        child: BlocBuilder<LoginBloc, LoginState>(
          builder: (context, state) {
            if (state is LoggedIn) {
              return MainLayoutChooser();
              // return FormLayoutChooser();
            }
            if (state is LoadingState) {
              return Center(child: CircularProgressIndicator());
            }
            if (state is LoggedOut) {
              return LoginLayoutChooser();
            }
            if (state is ConnectionError) {
              return LoginLayoutChooser();
            }
            if (state is UnknownState) {
              BlocProvider.of<LoginBloc>(context).add(AppLoaded());
              return Container(
                color: Colors.black87,
              );
            } else {
              return Text('Bug : voir login bloc');
            }
          },
        ),
      ),
    );
  }
}
