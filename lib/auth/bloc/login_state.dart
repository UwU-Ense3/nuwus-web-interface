part of 'login_bloc.dart';

@immutable
abstract class LoginState {}

class UnknownState extends LoginState {}

class LoggedIn extends LoginState {
  final String mail;
  final String password;

  LoggedIn(this.mail, this.password);
}

class LoggedOut extends LoginState {}

class ConnectionError extends LoginState {
  final Exception exception;
  final String cachedMail;

  ConnectionError(this.exception, this.cachedMail);
}

class LoadingState extends LoginState {}
