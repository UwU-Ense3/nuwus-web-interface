/// Ce fichier permet de gérer l'état d'authentification. Il prend en entrée des
/// événements (définis dans le fichier LoginEvent) que l'on peut appeler
/// depuis n'importe quel endroit de l'application et met à jour parmi ceux
/// définis dans LoginState l'état en fonction de ces événements.
import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:nuwus_web_interface/networking/api_helper.dart';

import '../auth_helper.dart';
import '../auth_saver.dart';

part 'login_event.dart';
part 'login_state.dart';

class LoginBloc extends Bloc<LoginEvent, LoginState> {
  final _authHelper = AuthHelper();

  LoginBloc() : super(UnknownState()) {
    on<UserLoginRequested>(_onUserLoginRequested);
    on<UserLogoutRequested>(_onUserLogoutRequested);
    on<AppLoaded>(_onAppLoaded);
  }

  void _onAppLoaded(event, emit) async {
// Start of the application
    if (event is AppLoaded) {
      var loginCredentials = await AuthSaver.loadUser();
      var mail = loginCredentials[0];
      var password = loginCredentials[1];
      // print(loginCredentials.toString());
      if ((mail == '' || mail == null) || (password == '' || password == null)) {
        print('Pas d\'identifiants trouvés');
        emit(LoggedOut());
      } else {
        try {
          var isCredentialValid = await _authHelper.login(mail, password);
          if (isCredentialValid) {
            print('User logged in : $mail');
            emit(LoggedIn(mail, password));
          } else {
            print('Utilisateur inconnu, logging out');
            AuthSaver.saveUser('', '');
            emit(LoggedOut());
          }
        } on HttpResponseException catch (e) {
          print('erreur $e');
          emit(LoggedOut());
        } on TimeoutException catch (e) {
          print('Erreur de communication avec le serveur : ' + e.toString());
          emit(ConnectionError(e, mail));
        }
      }
    }
  }

  void _onUserLoginRequested(UserLoginRequested event, Emitter<LoginState> emit) async {
    emit(LoadingState()); // affiche un écran de chargement pendant la communication avec le serveur
    try {
      var isCredentialValid = await _authHelper.login(event.mail, event.password);
      if (isCredentialValid) {
        print('User logged in : ${event.mail}');
        AuthSaver.saveUser(event.mail, event.password);
        emit(LoggedIn(event.mail, event.password));
      } else {
        print('Utilisateur inconnu, logging out');
        AuthSaver.saveUser('', '');
        emit(LoggedOut());
      }
    } on TimeoutException catch (e) {
      print('Erreur de communication avec le serveur : ' + e.toString());
      emit(ConnectionError(e, event.mail));
    } on HttpResponseException catch (e) {
      print('erreur $e');
      emit(ConnectionError(e, event.mail));
    }
  }

  void _onUserLogoutRequested(UserLogoutRequested event, Emitter<LoginState> emit) async {
    emit(LoadingState());
    await AuthSaver.saveUser('', '');
    emit(LoggedOut());
  }
}
