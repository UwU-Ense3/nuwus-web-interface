part of 'login_bloc.dart';

@immutable
abstract class LoginEvent {}

class AppLoaded extends LoginEvent {}

class UserLoginRequested extends LoginEvent {
  final String mail;
  final String password;

  UserLoginRequested(this.mail, this.password);
}

class UserLogoutRequested extends LoginEvent {}
