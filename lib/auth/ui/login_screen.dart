import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../bloc/login_bloc.dart';

class LoginScreen extends StatefulWidget {
  const LoginScreen({Key? key}) : super(key: key);

  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  final TextEditingController _mailController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();
  String? _validation;

  @override
  void dispose() {
    // Clean up the controller when the widget is disposed.
    _mailController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    var state = BlocProvider.of<LoginBloc>(context).state;

    if (state is ConnectionError) {
      _mailController.text = state.cachedMail;
    }
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 30),
      child: ListView(
        children: [
          Padding(
            padding: const EdgeInsets.only(top: 30),
            child: Text(
              "Connexion",
              style: Theme.of(context).textTheme.bodyText1,
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(top: 30),
            child: TextField(
              keyboardType: TextInputType.emailAddress,
              controller: _mailController,
              style: Theme.of(context).textTheme.bodyText1,
              autocorrect: false,
              decoration: InputDecoration(
                filled: true,
                labelText: "Adresse Email",
                errorText: _validation,
                // border: OutlineInputBorder(),
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(top: 30),
            child: TextField(
              keyboardType: TextInputType.visiblePassword,
              obscureText: true,
              controller: _passwordController,
              style: Theme.of(context).textTheme.bodyText1,
              autocorrect: false,
              onSubmitted: (_) {
                BlocProvider.of<LoginBloc>(context)
                    .add(UserLoginRequested(_mailController.text, _passwordController.text));
              },
              decoration: InputDecoration(
                filled: true,
                labelText: "Mot de passe",
                // border: OutlineInputBorder(),
              ),
            ),
          ),
          Padding(
            padding: EdgeInsets.only(top: 30),
            child: ElevatedButton(
              style: Theme.of(context).elevatedButtonTheme.style,
              onPressed: () {
                BlocProvider.of<LoginBloc>(context)
                    .add(UserLoginRequested(_mailController.text, _passwordController.text));
              },
              child: Text(
                "Connexion",
              ),
            ),
          ),
        ],
      ),
    );
  }
}
