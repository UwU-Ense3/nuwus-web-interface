import 'package:flutter/material.dart';
import 'package:nuwus_web_interface/auth/ui/login_screen.dart';

class LoginLayoutChooser extends StatelessWidget {
  const LoginLayoutChooser({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Connexion'),
      ),
      body: LayoutBuilder(builder: (context, constraints) {
        if (constraints.maxWidth > 600) {
          return WebLoginLayout();
        } else {
          return MobileLoginLayout();
        }
      }),
    );
  }
}

class WebLoginLayout extends StatefulWidget {
  const WebLoginLayout({Key? key}) : super(key: key);

  @override
  State<WebLoginLayout> createState() => _WebLoginLayoutState();
}

class _WebLoginLayoutState extends State<WebLoginLayout> {
  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Container(
          child: LoginScreen(),
          width: 600,
        ),
      ],
    );
  }
}

class MobileLoginLayout extends StatelessWidget {
  MobileLoginLayout({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return LoginScreen();
  }
}
