import 'package:shared_preferences/shared_preferences.dart';

// import 'package:flutter_secure_storage/flutter_secure_storage.dart';
//
// class AuthSaver {
//   static const String _keyToken = "auth_key";
//   static const String _keyMail = "mail";
//   static const _storage = FlutterSecureStorage();
//
//   static Future<List<String?>> loadUser() async {
//     String? userToken = await _storage.read(key: _keyToken);
//     String? mail = await _storage.read(key: _keyMail);
//     print("User loaded: mail: $mail, token: $userToken");
//     return [mail, userToken];
//   }
//
//   static Future<void> saveUser(String mail, String userToken) async {
//     print("Writing user: mail: $mail, token: $userToken");
//     _storage.write(key: _keyToken, value: userToken);
//     _storage.write(key: _keyMail, value: mail);
//   }
// }

class AuthSaver {
  static const String _keyToken = "auth_key";
  static const String _keyMail = "mail";

  static Future<List<String?>> loadUser() async {
    SharedPreferences _storage = await SharedPreferences.getInstance();
    String? userToken = (_storage.get(_keyToken)) as String?;
    String? mail = (_storage.get(_keyMail)) as String?;
    // print("User loaded: mail: $mail, token: $userToken");
    return [mail, userToken];
  }

  static Future<void> saveUser(String mail, String userToken) async {
    print("Writing user: mail: $mail, token: $userToken");
    SharedPreferences _storage = await SharedPreferences.getInstance();
    _storage.setString(_keyToken, userToken);
    _storage.setString(_keyMail, mail);
  }
}
