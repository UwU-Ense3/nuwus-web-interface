import 'package:nuwus_web_interface/networking/api_helper.dart';

class AuthHelper {
  final ApiHelper _api = ApiHelper();

  Future<bool> login(String mail, String password) async {
    Map<String, String> headers = {'mail': mail, 'password': password};
    try {
      await _api.post('api/login', '', headers: headers);
    } on HttpResponseException catch (e) {
      if (e.statusCode == 401) {
        return false;
      }
      rethrow;
    }
    return true;
  }
}
