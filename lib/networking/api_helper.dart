/// Ce fichier définit les méthodes GET et POST pour interagir avec l'api
import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:http/http.dart' as http;

class HttpResponseException extends HttpException {
  final int statusCode;

  HttpResponseException(this.statusCode, String message) : super(message);
}

class ApiHelper {
  // Url VPS :
  final String _baseUrl = "https://api.2uwu4u.club/";
  // final String _baseUrl = "http://178.170.40.196:5000/";

  // Url pour serveur local sur la meme machine que l'émulateur :
  // final String _baseUrl = "http://10.0.2.2:5000/";

  Future<dynamic> get(String url, {Map<String, String> headers = const <String, String>{}}) async {
    var uri = Uri.parse(_baseUrl + url);
    http.Response response;
    response = await http
        .get(
          uri,
          headers: <String, String>{
            'Content-Type': 'application/json',
            'charset': 'UTF-8',
            'Connection': 'keep-alive'
          }..addAll(headers),
        )
        .timeout(const Duration(seconds: 2));

    if (response.statusCode >= 200 && response.statusCode < 300) {
      // If success
      var responseJson = json.decode(response.body.toString());
      return responseJson;
    } else {
      throw HttpResponseException(
        response.statusCode,
        'Request failed: ${response.statusCode} \n ${response.body}',
      );
    }
  }

  Future<dynamic> post(String url, String jsonData,
      {Map<String, String> headers = const <String, String>{}}) async {
    http.Response response;
    var uri = Uri.parse(_baseUrl + url);
    response = await http.post(
      uri,
      headers: <String, String>{
        'Content-Type': 'application/json',
        'charset': 'UTF-8',
        "Connection": "keep-alive"
      }..addAll(headers),
      body: jsonData,
    )
        // .timeout(const Duration(seconds: 2))
        ;
    if (response.statusCode >= 200 && response.statusCode < 300) {
      var responseJson = json.decode(response.body.toString());
      return responseJson;
    } else {
      throw HttpResponseException(
        response.statusCode,
        'Erreur : ${response.body} (${response.statusCode})',
      );
    }
  }

  Future<dynamic> put(String url, String jsonData,
      {Map<String, String> headers = const <String, String>{}}) async {
    http.Response response;
    var uri = Uri.parse(_baseUrl + url);
    response = await http.put(
      uri,
      headers: <String, String>{
        'Content-Type': 'application/json',
        'charset': 'UTF-8',
        "Connection": "keep-alive"
      }..addAll(headers),
      body: jsonData,
    )
        // .timeout(const Duration(seconds: 2))
        ;
    if (response.statusCode >= 200 && response.statusCode < 300) {
      var responseJson = json.decode(response.body.toString());
      return responseJson;
    } else {
      throw HttpResponseException(
        response.statusCode,
        'Erreur : ${response.body} (${response.statusCode})',
      );
    }
  }

  Future<void> delete(String url, {Map<String, String> headers = const <String, String>{}}) async {
    http.Response response;
    var uri = Uri.parse(_baseUrl + url);
    response = await http.delete(
      uri,
      headers: <String, String>{
        'Content-Type': 'application/json',
        'charset': 'UTF-8',
        "Connection": "keep-alive"
      }..addAll(headers),
    )
        // .timeout(const Duration(seconds: 2))
        ;
    if (response.statusCode >= 200 && response.statusCode < 300) {
      return;
    } else {
      throw HttpResponseException(
        response.statusCode,
        'Erreur : ${response.body} (${response.statusCode})',
      );
    }
  }
}
