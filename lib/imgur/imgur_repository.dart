import 'dart:convert';
import "package:universal_html/html.dart" as html;

import 'package:nuwus_web_interface/imgur/imgur_helper.dart';

class ImgurRepository {
  var _client = ImgurHelper();

  Future<String> uploadImage(html.File file) async {
    // String base64Image = base64Encode(file.readAsBytesSync());
    final reader = html.FileReader();
    reader.readAsDataUrl(file);
    var encoded;
    await reader.onLoad.first.then((res) {
      encoded = reader.result as String;
    });
    final imageBase64 = encoded.replaceFirst(
        RegExp(r'data:image/[^;]+;base64,'), ''); // this is to remove some non necessary stuff
    var response = await _client.post('3/image', {'image': imageBase64});
    String link = json.decode(response)['data']['link'];
    return link;
  }
}
