import 'package:http/http.dart';

import 'imgur_secret.dart';

class ImgurHelper {
  final String _baseUrl = "https://api.imgur.com/";

  Future<String> post(String endpoint, Map<String, String> fields,
      {Map<String, String> headers = const <String, String>{}}) async {
    var baseHeaders = {'Authorization': 'Client-ID ${clientId}'};
    var uri = Uri.parse(_baseUrl + endpoint);

    var request = MultipartRequest('POST', uri);
    request.fields.addAll(fields);
    request.headers.addAll(baseHeaders..addAll(headers));
    request.headers.addAll({'Referrer-Policy': 'no-referrer'});
    print('upload en cours : $request');

    StreamedResponse response = await request.send();

    if (response.statusCode == 200) {
      return response.stream.bytesToString();
    } else {
      print(await response.stream.bytesToString());
      throw Exception('Erreur imgur : ${response.statusCode}');
    }
  }
}
