import 'dart:ui';

import 'package:dart_date/dart_date.dart';
import 'package:equatable/equatable.dart';
import 'package:intl/intl.dart';
import 'package:nuwus_web_interface/repository/event_repository.dart';

class Event {
  int? id;
  String name;
  DateTime beginTime;
  DateTime endTime;
  String location;
  EventType eventType;
  List<Organizer> organizers;

  // final List<User> participants;
  String description;
  List<Tag> tags;
  int numberParticipants;
  int price;
  String imageLink;

  static Event defaultEvent = Event(
    null,
    '',
    DateTime.now(),
    DateTime.now().add(Duration(hours: 1)),
    '',
    EventType(0, 'Autre', '0xFF9A3E3E'),
    [],
    '',
    [],
    0,
    '',
    0,
  );

  Event(this.id, this.name, this.beginTime, this.endTime, this.location, this.eventType, this.organizers,
      this.description, this.tags, this.numberParticipants, this.imageLink, this.price);

  factory Event.fromJson(Map<String, dynamic> json) {
    List<Organizer> organizers = [];
    for (Map<String, dynamic> orgJson in json['organizers']) {
      organizers.add(Organizer.fromJson(orgJson));
    }
    List<Tag> tags = [];
    for (Map<String, dynamic> tagJson in json['tags']) {
      tags.add(Tag.fromJson(tagJson));
    }
    return Event(
        json['id'],
        json['name'],
        DateTime.parse(json['begin_time']),
        DateTime.parse(json['end_time']),
        json['location'],
        EventType.fromJson(json['type']),
        organizers,
        json['description'],
        tags,
        json['n_participants'],
        json['image_link'],
        json['price']);
  }

  Event copy() {
    return Event(this.id, this.name, this.beginTime, this.endTime, this.location, this.eventType,
        this.organizers, this.description, this.tags, this.numberParticipants, this.imageLink, this.price);
  }

  String formatOrganizers() {
    var result = '';
    for (var i = 0; i < organizers.length; i++) {
      if (i > 0) {
        result += ', ';
      }
      result += organizers[i].name;
    }
    return result;
  }

  int durationInHours() {
    return endTime.difference(beginTime).inHours;
  }

  String formatFullDatetime({bool full = false}) {
    if (beginTime.day == endTime.day || durationInHours() < 13) {
      return relativeDate(beginTime, full: full) +
          DateFormat(' \'de \'HH:mm', 'fr_FR').format(beginTime) +
          DateFormat(' à HH:mm', 'fr_FR').format(endTime);
    }
    if (beginTime.month == endTime.month) {
      return DateFormat('\'Du \'dd', 'fr_FR').format(beginTime) +
          DateFormat('\' au \'dd MMMM', 'fr_FR').format(endTime);
    } else {
      return DateFormat('\'Du \'dd MMMM', 'fr_FR').format(beginTime) +
          DateFormat('\' au \'dd MMMM', 'fr_FR').format(endTime);
    }
  }

  String relativeDate(DateTime dateTime, {bool full = false}) {
    var today = DateTime.now().endOfDay;
    if (dateTime.isBefore(today.add(const Duration(days: -1)))) {
      return DateFormat('dd MMM', 'fr_FR').format(dateTime);
    }
    if (dateTime.isBefore(today)) {
      return 'Aujourd\'hui';
    }
    if (dateTime.isBefore(today.add(const Duration(days: 1)))) {
      return 'Demain';
    }
    if (dateTime.isBefore(today.add(const Duration(days: 6)))) {
      var text = DateFormat('EEEE', 'fr_FR').format(dateTime);
      return '${text[0].toUpperCase()}${text.substring(1)}';
    }
    if (dateTime.isBefore(today.endOfMonth)) {
      var text = DateFormat('EEEE dd', 'fr_FR').format(dateTime);
      return '${text[0].toUpperCase()}${text.substring(1)}';
    } else {
      if (full) {
        var text = DateFormat('EEEE dd MMMM', 'fr_FR').format(dateTime);
        return '${text[0].toUpperCase()}${text.substring(1)}';
      }
      return DateFormat('dd MMM', 'fr_FR').format(dateTime);
    }
  }

  Map<String, dynamic> toJson() => {
    "begin_time": beginTime.toIso8601String(),
    "description": description,
    "end_time": endTime.toIso8601String(),
    "image_link": imageLink,
    "location": location,
    "n_participants": numberParticipants,
    "name": name,
    "organizers": organizers.map((e) => e.name).toList(),
    "price": price,
    "tags": tags.map((e) => e.name).toList(),
    "type": eventType.name,
  };

  @override
  String toString() {
    return "[$id] Event $name : $eventType \n"
        "   $beginTime - $endTime\n"
        "   location : $location\n"
        "   organizers : $organizers\n"
        "   price : $price\n"
        "   image : $imageLink\n"
        "   number participants : $numberParticipants\n"
        "   description : $description\n"
        "   tags : $tags\n";
  }
}

class EventType extends Equatable {
  int id;
  String name;
  String color;

  EventType(this.id, this.name, this.color);

  factory EventType.fromJson(Map<String, dynamic> json) {
    return EventType(json['id'], json['name'], json['color']);
  }

  @override
  String toString() {
    return "EventType $name";
  }

  @override
  List<Object> get props => [id];
}

class Organizer {
  final int? id;
  final String name;
  static Map<int, Future<Organizer>> lookupDic = {};

  const Organizer(this.id, this.name);

  factory Organizer.fromJson(Map<String, dynamic> json) {
    return Organizer(json['id'], json['name']);
  }

  static Future<Organizer> organizerFromId(int id) {
    if (lookupDic.containsKey(id)) {
      return lookupDic[id] as Future<Organizer>;
    }
    var repository = OrganizerRepository();
    var org = repository.fetchOrganizer(id);
    lookupDic[id] = org;
    return org;
  }

  @override
  String toString() {
    return name;
  }

}

class Tag {
  final int? id;
  final String name;
  final String? color;

  static Map<int, Future<Tag>> lookupDic = {};

  const Tag(this.id, this.name, this.color);

  factory Tag.fromJson(Map<String, dynamic> json) {
    return Tag(json['id'], json['name'], json['color']);
  }

  static Future<Tag> tagFromId(int id) {
    if (lookupDic.containsKey(id)) {
      return lookupDic[id] as Future<Tag>;
    }
    var repository = TagRepository();
    var org = repository.fetchTag(id);
    lookupDic[id] = org;
    return org;
  }

  @override
  String toString() {
    return name;
  }
}

/// pour pouvoir utiliser des string dans Color() au lieu d'un int
extension HexColor on Color {
  /// String is in the format "aabbcc" or "ffaabbcc" with an optional leading "0xff".
  static Color fromHex(String hexString) {
    final buffer = StringBuffer();
    if (hexString.length == 6 || hexString.length == 7) buffer.write('ff');
    buffer.write(hexString.replaceFirst('0xff', '').replaceFirst('0xFF', ''));
    return Color(int.parse(buffer.toString(), radix: 16)).withOpacity(1);
  }

  /// Prefixes a hash sign if [leadingHashSign] is set to `true` (default is `true`).
  String toHex({bool leadingHashSign = true}) => '${leadingHashSign ? 'oxff' : ''}'
      '${alpha.toRadixString(16).padLeft(2, '0')}'
      '${red.toRadixString(16).padLeft(2, '0')}'
      '${green.toRadixString(16).padLeft(2, '0')}'
      '${blue.toRadixString(16).padLeft(2, '0')}';
}
